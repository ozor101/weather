@echo off
@setlocal

set _PATH=%~dp0

if "%PHP_COMMAND%" == "" set PHP_COMMAND=C:/Server/nginx-1.10.2/php-7.0.13/php.exe

"%PHP_COMMAND%" "%_PATH%unit" %*

@endlocal