<?php

class TestWeather extends \Test\TestCase
{
    /**
     * @var \PDO
     */
    protected $pdo;

    public function up()
    {
        parent::up();

        if (empty($this->pdo) || !$this->pdo instanceof \PDO) {
            global $pdo;
            if (!$pdo instanceof \PDO) {
                include __DIR__ . '/../../../config/database.php';
            }
            $this->pdo = $pdo;
        }
    }

    public function test_insert_works_correctly()
    {
        $row_count = $this->getCount($this->pdo);

        $weather = new \Weather\Weather($this->pdo);
        $weather->insert(30, "Novosibirsk");

        $this->assertEquals($this->getCount(), ($row_count+1));

        $stmt = $this->pdo->query('SELECT city FROM weather WHERE id = last_insert_id()');
        $result = $stmt->fetchColumn();
        $this->assertEquals($result, "Novosibirsk");
    }

    public function test_getMaxTemperature_works_correctly()
    {
        $weather = new \Weather\Weather($this->pdo);
        $temperature = $weather->getMaxTemperature();

        $this->assertNotEmpty($temperature);
        $this->assertTrue(floatval($temperature) !== 0);

        $stmt = $this->pdo->query('SELECT * FROM weather WHERE weather = ' . floatval($temperature));
        $this->assertTrue($stmt->rowCount() > 0);
    }

    public function test_getMinTemperature_works_correctly()
    {
        $weather = new \Weather\Weather($this->pdo);
        $temperature = $weather->getMinTemperature();

        $this->assertNotEmpty($temperature);
        $this->assertTrue(floatval($temperature) !== 0);

        $stmt = $this->pdo->query('SELECT * FROM weather WHERE weather = ' . floatval($temperature));
        $this->assertTrue($stmt->rowCount() > 0);
    }

    public function test_min_vs_max()
    {
        $weather = new \Weather\Weather($this->pdo);
        $weather->insert(30, "Novosibirsk");
        $weather->insert(50, "Novosibirsk");

        $max = $weather->getMaxTemperature();
        $min = $weather->getMinTemperature();

        $this->assertTrue($max > $min);
    }

    public function test_getAll_works_correctly()
    {
        $weather = new \Weather\Weather($this->pdo);

        $list_before = $weather->getAll();

        $weather->insert(10, "Novosibirsk");
        $weather->insert(25, "Novosibirsk");

        $list = $weather->getAll();

        $this->assertNotEmpty($list);
        $this->assertTrue(count($list) == $this->getCount());
        $this->assertTrue(count($list) > count($list_before));
    }


    protected function getCount()
    {
        $stmt = $this->pdo->query('SELECT * FROM weather');
        return $stmt->rowCount();
    }
}