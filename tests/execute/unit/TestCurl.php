<?php

class TestCurl extends \Test\TestCase
{
    public function test_fetch_returns_correct_result()
    {
        $curl = new \Weather\Curl();
        $result = $curl->fetch(
            'http://api.openweathermap.org/data/2.5/weather?q=Moscow,ru&appid=32faf0b7000bfd392e1a9479cd3d5a5b&units=metric'
        );

        $this->assertNotEmpty($result);
        $this->assertType($result, 'string');

        $answer = json_decode($result);
        $this->assertType($answer, 'object');

        $this->assertNotEmpty($answer->main->temp);
    }
}