<?php

namespace Test;

class Unit
{
    public static function execute($testCase)
    {
        $failed = [];
        /**
         * @var TestCase $instance
         */
        $instance = new $testCase;
        foreach (get_class_methods($testCase) as $method) {
            if (!in_array($method, ['up', 'down', 'registerCase'])) {
                call_user_func([$instance, 'registerCase'], [$testCase, $method]);
                call_user_func([$instance, 'up']);

                echo "Test $testCase:$method started\n";
                $timer = time();

                call_user_func([$instance, $method]);

                $executionTime = time() - $timer;
                echo "Test $testCase:$method finished. Execution time: $executionTime seconds\n\n";

                call_user_func([$instance, 'down']);

                if (!empty($instance->failedTests)) {
                    $failed = array_merge($failed, $instance->failedTests);
                }
            }
        }

        if (!empty($failed)) {
            echo "Some tests are failed:\n\n";
            foreach ($failed as $item) {
                echo 'Failed test ' . implode(":", $item['test']) . ' with message: ' . $item['message'] . "\n";
                foreach ($item['trace'] as $key => $trace) {
                    if (!empty($trace['file']) && !empty($trace['line'])) {
                        echo '#' . $key . ') ' . $trace['file'] . ':' . $trace['line'] . "\n";
                    }
                }
            }
            echo "\n";
        } else {
            echo "All tests in case $testCase are successful\n\n";
        }
    }
}