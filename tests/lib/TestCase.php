<?php

namespace Test;

class TestCase
{
    public $case;
    public $failedTests = [];

    public function up()
    {
    }

    public function down()
    {
    }

    public function registerCase($case)
    {
        $this->case = $case;
    }

    protected final function assertTrue($value, $message = null)
    {
        if ($value !== true) {
            $this->fail($message ?: 'Failed asserting for true');
        }
    }

    protected final function assertFalse($value, $message = null)
    {
        if ($value !== false) {
            $this->fail($message ?: 'Failed asserting for false');
        }
    }

    protected final function assertType($value, $type, $message = null)
    {
        if (gettype($value) != $type) {
            $this->fail($message ?: 'Failed asserting for type');
        }
    }

    protected final function assertNull($value, $message = null)
    {
        if ($value !== null) {
            $this->fail($message ?: 'Failed asserting for null');
        }
    }

    protected final function assertNotNull($value, $message = null)
    {
        if ($value === null) {
            $this->fail($message ?: 'Failed asserting for not null');
        }
    }

    protected final function assertEmpty($value, $message = null)
    {
        if (!empty($value)) {
            $this->fail($message ?: 'Failed asserting for empty');
        }
    }

    protected final function assertNotEmpty($value, $message = null)
    {
        if (empty($value)) {
            $this->fail($message ?: 'Failed asserting for not empty');
        }
    }

    protected final function assertNotIn($value, $array, $message = null)
    {
        if (!in_array($value, $array)) {
            $this->fail($message ?: 'Failed asserting for in array');
        }
    }

    protected final function assertNotEquals($value1, $value2, $message = null)
    {
        if ($value1 == $value2) {
            $this->fail($message ?: 'Failed asserting for values not equal');
        }
    }

    protected final function assertEquals($value1, $value2, $message = null)
    {
        if ($value1 != $value2) {
            $this->fail($message ?: 'Failed asserting for values equal');
        }
    }

    protected function fail($message)
    {
        try {
            throw new \Exception($message);
        } catch(\Exception $e) {
            $this->failedTests[] = [
                'test' => $this->case,
                'message' => $message,
                'trace' => $e->getTrace()
            ];
        }

    }
}