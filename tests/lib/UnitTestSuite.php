<?php

namespace Test;

class UnitTestSuite
{
    protected $pathUnit = __DIR__ . '/../execute/unit/';
    protected $testUnit = [];

    public function __construct($config)
    {
        $case = simplexml_load_file($config);
        foreach ((array)$case->unit->case as $item) {
            $this->addUnitTest($item);
        }
    }

    public function addUnitTest($test)
    {
        $this->testUnit[] = $test;
    }

    public function runUnitTests()
    {
        foreach ($this->testUnit as $testCase) {
            require_once $this->pathUnit . $testCase . ".php";
            Unit::execute($testCase);
        }
    }
}