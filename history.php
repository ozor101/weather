<?php

require_once __DIR__ . '/config/config.php';

/**
 * @var array $api
 */
$city = $api['data']['city'];
$weather = new \Weather\Weather($pdo);

$minTempValue = $weather->getMinTemperature();
$maxTempValue = $weather->getMaxTemperature();


$baseUrl = '/history.php';
if (!empty($_GET['q'])) {
    $q = htmlspecialchars($_GET['q']);
    $complexUrl = $baseUrl . '?' . $q;
} else {
    $q = 'weather';
}

$temps = $weather->getAll( ($q == 'weather') ? 'weather' : 'date' );

include './views/history.php';
