<?php

$dbParams = [
    'host' => 'localhost',
    'dbname' => 'openweathermap',
    'username' => 'root',
    'password' => 'mysql',
    'charset' => 'utf8',
];

$pdo = new \PDO(
    sprintf('mysql:host=%s;dbname=%s', $dbParams['host'], $dbParams['dbname']),
    $dbParams['username'],
    $dbParams['password']
);
