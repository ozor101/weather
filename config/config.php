<?php

$files = scandir(__DIR__);
foreach ($files as $file) {
    if ($file != "." && $file != ".." && $file != "config.php" && strpos($file, ".php") !== false) {
        require $file;
    }
}

spl_autoload_register(function ($class)
{
    $spaces = [
        'Weather\\' => __DIR__ . '/../class/',
        'Test\\' => __DIR__ . '/../tests/lib/',
    ];

    foreach ($spaces as $prefix => $base_dir) {
        $len = strlen($prefix);
        if (strncmp($prefix, $class, $len) !== 0) {
            continue;
        }

        $relative_class = substr($class, $len);
        $file = $base_dir . str_replace('\\', '/', $relative_class) . '.php';

        if (file_exists($file)) {
            require_once $file;
            return;
        }
    }
});