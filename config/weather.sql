
--
-- База данных: `openweathermap`
--

-- --------------------------------------------------------

--
-- Структура таблицы `weather`
--

CREATE TABLE `weather` (
  `id` int(10) UNSIGNED NOT NULL,
  `weather` float NOT NULL,
  `date` datetime NOT NULL,
  `city` varchar(40) NOT NULL DEFAULT 'Moscow'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `weather`
--
ALTER TABLE `weather`
  ADD PRIMARY KEY (`id`),
  ADD KEY `weather` (`weather`),
  ADD KEY `date` (`date`);

--
-- AUTO_INCREMENT для таблицы `weather`
--
ALTER TABLE `weather`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;