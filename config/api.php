<?php

$api = [
    'data' => [
        'appid' => '32faf0b7000bfd392e1a9479cd3d5a5b',
        'city' => 'Moscow',
        'country' => 'ru',
        'units' => 'metric'
    ],
    'target' => [
        'current' => 'http://api.openweathermap.org/data/2.5/weather?q={city},{country}&appid={appid}&units={units}',
        'history' => 'http://history.openweathermap.org/data/2.5/history/city?q={city},{country}&appid={appid}&units={units}',
    ]
];