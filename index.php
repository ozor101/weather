<?php

require_once __DIR__ . '/config/config.php';

/**
 * @var array $api
 */
$target = $api['target']['current'];
foreach ($api['data'] as $key => $data) {
    $target = str_ireplace('{'.$key.'}', $data, $target);
}
$result = json_decode((new \Weather\Curl())->fetch($target));
$temp = $result->main->temp;

$id = (new \Weather\Weather($pdo))->insert($result->main->temp, $api['data']['city']);
echo "Inserted row with id = $id";
