<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Монитор истории температуры</title>
</head>
<body>

<div>
    Город: <b><?=ucfirst($city);?></b>
</div>

<div>
    <div>Сортировать по:</div>
    <div><a href="<?="{$baseUrl}?q=weather"; ?>">Температуре</a></div>
    <div><a href="<?="{$baseUrl}?q=history"; ?>">Хронологии замеров</a></div>
</div>

<table>
    <thead>
        <tr>
            <td>Дата</td>
            <td>Температура</td>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($temps as $temp) : ?>
        <tr>
            <td><?=date('d.m.Y H:i:s', strtotime($temp['date']));?></td>
            <td><?=$temp['weather'];?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>


<div>
    <h3>Минимальная температура</h3>
    <div><?=$minTempValue;?></div>
</div>
<div>
    <h3>Максимальная температура</h3>
    <div><?=$maxTempValue;?></div>
</div>


</body>
</html>