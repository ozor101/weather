<?php

namespace Weather;

use PDO;

class Weather
{
    /**
     * @var PDO
     */
    protected $pdo;

    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * @return string
     */
    public function insert($temperature, $city) : string
    {
        $stmt = $this->pdo->prepare("insert into weather (weather, date, city) values (?, ?, ?)");
        $stmt->execute([
            $temperature, date('Y-m-d H:i:s'), $city
        ]);
        return $this->pdo->lastInsertId();
    }

    /**
     * @return string
     */
    public function getMaxTemperature() : string
    {
        $stmt = $this->pdo->query('SELECT weather FROM weather ORDER BY weather DESC LIMIT 1');
        return $stmt->fetchColumn();
    }

    /**
     * @return string
     */
    public function getMinTemperature() : string
    {
        $stmt = $this->pdo->query('SELECT weather FROM weather ORDER BY weather ASC LIMIT 1');
        return $stmt->fetchColumn();
    }

    /**
     * @param string $sortBy
     * @return array
     */
    public function getAll(string $sortBy = 'weather') : array
    {
        $stmt = $this->pdo->query("SELECT * FROM weather GROUP BY id ORDER BY $sortBy DESC");
        return $stmt->fetchAll();
    }
}