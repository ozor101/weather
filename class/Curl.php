<?php

namespace Weather;

class Curl
{
    /**
     * @var array
     */
    private $curlOptions;

    public function __construct($curlOptions = [])
    {
        $this->curlOptions = $curlOptions;
    }

    public function fetch($url)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt_array($ch, $this->curlOptions);
        
        $content = curl_exec($ch);
        curl_close($ch);

        return $content;
    }
}
